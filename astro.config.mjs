import { defineConfig } from 'astro/config';
import tailwind from "@astrojs/tailwind";
import icon from "astro-icon";

import sitemap from "@astrojs/sitemap";

// https://astro.build/config
export default defineConfig({
  site: 'https://winter-cat.com',
  integrations: [tailwind(), icon(), sitemap()],
  vite: {
    build: {
      rollupOptions: {
        output: {
          assetFileNames: 'assets/[hash][extname]',
          chunkFileNames: 'chunks/[hash].js',
          entryFileNames: 'entries/[hash].js',
          compact: true,
          sourcemap: false
        }
      }
    }
  }
});